rm(list = ls())
gc(reset = TRUE)

library(tidyverse, warn.conflicts = FALSE)
library(ggplot2)
library(ggthemes)
library(ggupset)

load_off <- function(x) {
  readRDS(x)$offTargets
}
ctr <- load_off("offtargets/site4_pBLANK.RDS")
norm_off <- function(x, ctr) x[!x$offTarget_sequence %in% ctr$offTarget_sequence, ]
# focus on site 4 for now
site4_tsai <- as.data.frame(data.table::fread("Tsai-HEK293_site4.dat"))
colnames(site4_tsai) <- c("gRNA", "offTarget_sequence", "peak_score")

pWT_s4 <- norm_off(load_off("offtargets/site4_pWT_site4.RDS"), ctr)
pP3_s4 <- norm_off(load_off("offtargets/site4_pP3_site4.RDS"), ctr)
tWT_s4 <- norm_off(load_off("offtargets/site4_tWT_site4.RDS"), ctr)
tP3_s4 <- norm_off(load_off("offtargets/site4_tP3_site4.RDS"), ctr)


mergeXY <- function(x, y, xn, yn, imp = c("peak_score", "offTarget_sequence")){
  if (!is.na(xn)) x <- x[, imp]
  xy <- merge(x, y[, imp],
              by = c("offTarget_sequence"), all = T)
  if (!is.na(xn)) {
    names(xy)[which(names(xy) == "peak_score.x")] <- xn
  } else {
    names(xy)[which(names(xy) == "peak_score")] <- yn
    return(xy)
  }
  if (!is.na(yn)) names(xy)[which(names(xy) == "peak_score.y")] <- yn
  return(xy)
}

site4 <- mergeXY(pP3_s4, pWT_s4, "pP3", "pWT")
#site4 <- mergeXY(site4, tP3_s4, NA, "tP3")
#site4 <- mergeXY(site4, tWT_s4, NA, "tWT")
site4 <- mergeXY(site4, site4_tsai, NA, "WT")

# site4_ref <- "GGCACTGCGGCTGGAGGTGGGGG"
# is_ref <- site4$offTarget_sequence == site4_ref

# unroll
site4 <- site4 %>%
  gather("Condition", "Score", -offTarget_sequence) %>%
  #filter(Score > 100) %>%
  group_by(Condition) %>%
  mutate(Total = sum(Score, na.rm = T),
         Freq = Score / Total) %>%
  filter(!is.na(Score))

# not direct but prettier


# direct and not sorted anymore
ss4 <- site4 %>%
  group_by(offTarget_sequence) %>%
  mutate(Condition = sapply(list(Condition), function(x) paste0(sort(x), collapse="&")),
         ccount = length(Condition)) %>%
  summarize(
    Condition = unique(Condition),
    Freq = sum(Freq, na.rm = T),
    ccount = unique(ccount)) %>%
  group_by(Condition) %>%
  summarize(Freq = sum(Freq, na.rm = T) / unique(length(Condition)),
            Count = n(),
            ccount = unique(ccount))
ss4$Freq[ss4$ccount != 1] <- NA

# ggplot(ss4, aes(x = reorder(Condition, -Count), y = Count)) +
#   geom_bar(stat = "identity") +
#   geom_text(stat = 'identity',
#             aes(label = Count, y = Count, x = Condition, vjust = -1)) +
#   # geom_text(stat = 'identity',
#   #           aes(label = round(Freq, 3),
#   #               y = Count, x = Condition, vjust = -3)) +
#   axis_combmatrix(sep = "&")


# make ultimate comparison plot
library(UpSetR)

up <- list()
for (i in seq_len(dim(ss4)[1])) {
  up[ss4$Condition[i]] <- ss4$Count[i]
}

pdf(file = "upset_site4_p.pdf")
upset(fromExpression(up), order.by = "freq")
dev.off()



# RNF2
ctr_r2 <- data.table()
pWT_r2 <- norm_off(load_off("offtargets/RNF2_pWT_RNF2.RDS"), ctr_r2)
pP3_r2 <- pWT_r2[pWT_r2$offTarget == "no data here", ]
tWT_r2 <- norm_off(load_off("offtargets/RNF2_tWT_RNF2.RDS"), ctr_r2)
tP3_r2 <- norm_off(load_off("offtargets/RNF2_tP3_RNF2.RDS"), ctr_r2)
rnf2 <- mergeXY(pP3_r2, pWT_r2, "pP3", "pWT")
#rnf2 <- mergeXY(rnf2, tP3_r2, NA, "tP3")
#rnf2 <- mergeXY(rnf2, tWT_r2, NA, "tWT")
rnf2 <- mergeXY(rnf2, pP3_r2, NA, "WT") # WT has also no off-targets

# unroll
rnf2 <- rnf2 %>%
  gather("Condition", "Score", -offTarget_sequence) %>%
  #filter(Score > 100) %>%
  group_by(Condition) %>%
  mutate(Total = sum(Score, na.rm = T),
         Freq = Score / Total) %>%
  filter(!is.na(Score))

# not direct but prettier


# direct and not sorted anymore
rnf2 <- rnf2 %>%
  group_by(offTarget_sequence) %>%
  mutate(Condition = sapply(list(Condition), function(x) paste0(sort(x), collapse="&")),
         ccount = length(Condition)) %>%
  summarize(
    Condition = unique(Condition),
    Freq = sum(Freq, na.rm = T),
    ccount = unique(ccount)) %>%
  group_by(Condition) %>%
  summarize(Freq = sum(Freq, na.rm = T) / unique(length(Condition)),
            Count = n(),
            ccount = unique(ccount))
ss4$Freq[ss4$ccount != 1] <- NA

up <- list()
for (i in seq_len(dim(rnf2)[1])) {
  up[rnf2$Condition[i]] <- rnf2$Count[i]
}

pdf(file = "upset_rnf2_p.pdf")
upset(fromExpression(up), order.by = "freq")
dev.off()
