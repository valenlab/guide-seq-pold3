---
title: "GUIDE-seq for Zhuokun"
author: "Kornel Labun"
date: "10/22/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_knit$set(root.dir = "~/Projects/uib/crispr/gemma_zhuokun_emma/")
``` 

```{r load, include=FALSE}
library(GUIDEseq)
library(data.table)
library(ggplot2)
library(ggthemes)
library(ggVennDiagram)

load_off <- function(x) readRDS(x)$offTargets
norm_off <- function(x, ctr) x[!x$offTarget_sequence %in% ctr$V2, ]

# site 4
tsai_s4 <- fread("Tsai-HEK293_site4.dat", skip = 1)
ctr_s4 <- rbind(
  load_off("offtargets/site4_pBLANK.RDS"),
  load_off("offtargets/site4_tP3_ONLY.RDS"))
pWT_s4 <- norm_off(load_off("offtargets/site4_pWT_site4.RDS"), ctr_s4)
pP3_s4 <- norm_off(load_off("offtargets/site4_pP3_site4.RDS"), ctr_s4)
tWT_s4 <- norm_off(load_off("offtargets/site4_tWT_site4.RDS"), ctr_s4)
tP3_s4 <- norm_off(load_off("offtargets/site4_tP3_site4.RDS"), ctr_s4)
# RNF2
tsai_r2 <- fread("Tsai-K562_RNF2.dat", skip = 1)
tsai_r22 <- fread("Tsai-U2OS_RNF2.dat", skip = 1)
ctr_r2 <- rbind(
  load_off("offtargets/RNF2_tP3_ONLY.RDS"),
  load_off("offtargets/RNF2_tWT_ONLY.RDS"))
pWT_r2 <- norm_off(load_off("offtargets/RNF2_pWT_RNF2.RDS"), ctr_r2)
pP3_r2 <- norm_off(load_off("offtargets/RNF2_tP3_ONLY.RDS"), ctr_r2)
tWT_r2 <- norm_off(load_off("offtargets/RNF2_tWT_RNF2.RDS"), ctr_r2)
tP3_r2 <- norm_off(load_off("offtargets/RNF2_tP3_RNF2.RDS"), ctr_r2)
```

## Data processing

I have used custom script to extract UMI from the de-multiplexed fastq reads. In the next step adapters were trimmed with the use of cutadapt (against TTGAGTTGTCATATGTTAATAACGGTAT, ACATATGACAACTCAATTAAAC). Afterwards data was aligned to the human genome hg38 with bowtie 2 (with options --local --very-sensitive-local). GUIDE-seq (v1.18) package from Bioconductor was used to call off-targets with the default settings. Final off-targets are normalized against controls.

## Off-target overlap between Cas9 type for HEK-WT cells

WT K562/WT U2OS/WT HEK256 are off-targets extracted from original Tsai et al. GUIDE-seq paper for the site 4 and RNF2.  

We find big overlap between Cas9 type off-targets for site 4, for RNF2 we have not detected many off-targets for specialized Cas9 types.

```{r HEK-WTcells}
x <- list(`pWT` = unique(pWT_s4$offTarget_sequence),
          `pP3` = unique(pP3_s4$offTarget_sequence),
          `WT HEK293` = unique(tsai_s4$V2))

ggVennDiagram(x, lty = "dashed", color = "black", size = 1) +
  labs(title = "Site 4")

x <- list(`pWT` = unique(pWT_r2$offTarget_sequence),
          `pP3` = unique(pP3_r2$offTarget_sequence),
          `WT K562` = unique(tsai_r2$V2),
          `WT U2OS` = unique(tsai_r22$V2))
ggVennDiagram(x, lty = "dashed", color = "black", size = 1) +
  labs(title = "RNF2")
```

## Off-target overlap between Cas9 type for Tet-Cas9-XRCC2 cells

RNF2 seems to have very little off-targets in our data. Site 4 for Tet-Cas9-XRCC2 cells has less unique off-targets in comparison with original Tsai paper.

```{r Tet-Cas9-XRCC2cells}
x <- list(`tWT` = unique(tWT_s4$offTarget_sequence),
          `tP3` = unique(tP3_s4$offTarget_sequence),
          `WT HEK293` = unique(tsai_s4$V2))
ggVennDiagram(x, lty = "dashed", color = "black", size = 1) +
  labs(title = "Site 4")

x <- list(`tWT` = unique(tWT_r2$offTarget_sequence),
          `tP3` = unique(tP3_r2$offTarget_sequence),
          `WT K562` = unique(tsai_r2$V2),
          `WT U2OS` = unique(tsai_r22$V2))
ggVennDiagram(x, lty = "dashed", color = "black", size = 1) +
  labs(title = "RNF2")
```

## Off-target overlap between cell type

RNF2 is identical between different cell types and is not very informative. Site 4 shows differences between cell types and Cas9 types.

```{r cell_type}
x <- list(`tWT` = unique(tWT_s4$offTarget_sequence),
          `pWT` = unique(pWT_s4$offTarget_sequence),
          `WT HEK293` = unique(tsai_s4$V2))
ggVennDiagram(x, lty = "dashed", color = "black", size = 1) +
  labs(title = "Site 4")

x <- list(`tWT` = unique(tWT_r2$offTarget_sequence),
          `pWT` = unique(pWT_r2$offTarget_sequence),
          `WT K562` = unique(tsai_r2$V2),
          `WT U2OS` = unique(tsai_r22$V2))
ggVennDiagram(x, lty = "dashed", color = "black", size = 1) +
  labs(title = "RNF2")

x <- list(`tP3` = unique(tP3_s4$offTarget_sequence),
          `pP3` = unique(pP3_s4$offTarget_sequence),
          `WT HEK293` = unique(tsai_s4$V2))
ggVennDiagram(x, lty = "dashed", color = "black", size = 1) +
  labs(title = "Site 4")

x <- list(`tP3` = unique(tP3_r2$offTarget_sequence),
          `pP3` = unique(pP3_r2$offTarget_sequence),
          `WT K562` = unique(tsai_r2$V2),
          `WT U2OS` = unique(tsai_r22$V2))
ggVennDiagram(x, lty = "dashed", color = "black", size = 1) +
  labs(title = "RNF2")
```


## What off-targets do we have?  

Numeric values represent peak score, higher means more reads adhere to the peak detection rules.

# RNF2

``` {r tables}
imp <- c("peak_score", "offTarget_sequence")
mergeXY <- function(x, y, xn, yn){
  if (!is.na(xn)) x <- x[, imp]
  xy <- merge(x, y[, imp], 
              by = c("offTarget_sequence"), all = T)
  if (!is.na(xn)) {
    names(xy)[which(names(xy) == "peak_score.x")] <- xn
  } else {
    names(xy)[which(names(xy) == "peak_score")] <- yn
    return(xy)
  }
  if (!is.na(yn)) names(xy)[which(names(xy) == "peak_score.y")] <- yn
  return(xy)
}
rnf2 <- mergeXY(pP3_r2, pWT_r2, "pP3", "pWT")
rnf2 <- mergeXY(rnf2, tP3_r2, NA, "tP3")
rnf2 <- mergeXY(rnf2, tWT_r2, NA, "tWT")
#rnf2 <- mergeXY(rnf2, tWT_r2, NA, "WT ")
knitr::kable(rnf2, "pipe")
```

# Site 4

``` {r tabless4}
site4 <- mergeXY(pP3_s4, pWT_s4, "pP3", "pWT")
site4 <- mergeXY(site4, tP3_s4, NA, "tP3")
site4 <- mergeXY(site4, tWT_s4, NA, "tWT")
#site4 <- mergeXY(site4, tWT_s4, NA, "WT ")
knitr::kable(site4, "pipe")
```
