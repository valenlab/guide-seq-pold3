#!/bin/bash

qtdir=../fastq/fastq_trimed
qdir=../fastq
alndir=../align
mergeddir=../align/merged
offtdir=../offtargets
gdir=~/Projects/uib/crispr/chopchop_genomes/hg38v34.fa
bt2idx=~/Projects/uib/crispr/chopchop_genomes/hg38v34
th=6

function sam_to_bam(){
        head=$(dirname "$1")
        head=$(cd "$head" && pwd)
        name=$(basename $1)
        samtools view -Sb "$1" > "$head/${name%.*}.bam"
        samtools sort "$head/${name%.*}.bam" -O BAM -o "$head/${name%.*}_sorted.bam"
        rm "$head/${name%.*}.bam"
        samtools index "$head/${name%.*}_sorted.bam"
        samtools flagstat "$head/${name%.*}_sorted.bam" > "$head/${name%.*}_sorted.stat"
};

# make directory
#rm -rf $qtdir
#mkdir $qtdir

# trim adapters
for fn in $qdir/*; do
    cutadapt -b blue=TTGAGTTGTCATATGTTAATAACGGTAT -b red=ACATATGACAACTCAATTAAAC -b\
   	blueRevComp=ATACCGTTATTAACATATGACAACTCAA -b redRevComp=GTTTAATTGAGTTGTCATATGT -n 4 $fn > $qtdir/$(basename $fn)
done

# align our samples bwtie2
for fn in $qtdir/*R1*; do
    out=$(basename $fn)
    out=$alndir/${out//_S*/.sam}
    bowtie2 -p $th --local --very-sensitive-local -x $bt2idx -1 $fn -2 ${fn//R1/R2} -S $out
    sam_to_bam $out
done

# align our samples
#for fn in $qtdir/*R1*; do
#    out=$(basename $fn)
#    out=$alndir/${out//_S*/.sam}
#    bwa mem -t $th $gdir $fn ${fn//R1/R2} > $out
#    sam_to_bam $out
#done

#rm -rf $mergeddir $offtdir
#mkdir $mergeddir $offtdir

# merge sense, antisense
#for fn in $alndir/*_ng_sorted.bam; do
#    out=$(basename $fn)
#    out=$mergeddir/${out//_ng_sorted.bam/.bam}
#    samtools merge $out $fn ${fn//ng/ps}
#    samtools view -h $out > ${out//.bam/.sam}
#    rm $out
#done

# call offtargets using guideseq pipeline from Tsai
#source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
#export WORKON_HOME=/home/ai/.virtualenvs
#export PIP_VIRTUALENV_BASE=/home/ai/.virtualenvs
#workon guideseq

#pBLANK.sam  pP3_RNF2.sam   pWT_RNF2.sam   tP3_BLANK.sam  tP3_RNF2.sam   tWT_BLANK.sam  tWT_RNF2.sam
#pONLY.sam   pP3_site4.sam  pWT_site4.sam  tP3_ONLY.sam   tP3_site4.sam  tWT_ONLY.sam   tWT_site4.sam

#GGCACTGCGGCTGGAGGTGGNGG

#RNF2 is “GTCATCTTAGTCATTACCTGNGG”, and for HEK site4 is “GGCACTGCGGCTGGAGGTGGNGG”

#python ./soft/guideseq/guideseq/guideseq.py identify --aligned $mergeddir/pBLANK.sam --genome $gdir --outfolder $offtdir --target_sequence GGCACTGCGGCTGGAGGTGGNGG --description pBLANK_site4

#python ./soft/guideseq/guideseq/guideseq.py identify --aligned $mergeddir/pWT_site4.sam --genome $gdir --outfolder $offtdir --target_sequence GGCACTGCGGCTGGAGGTGGNGG --description pBLANK_site4

#python ./soft/guideseq/guideseq/guideseq.py visualize --infile $offtdir/identified/pWT_site4_identifiedOfftargets.txt --outfolder $offtdir --title pWT_site4



# should be substract??!!
# bedtools intersect -a sample -b control > output 

 #python guideseq/guideseq.py visualize --infile test/data/identified/EMX1_identifiedOfftargets.txt\
 #    --outfolder test/output/ --title EMX1



#python UMIbarcode_python2.py ../fastq/pWT_site4_ps_S1_R1_001.fastq.gz > pWT_site4_ps_R1_UMI.txt


# lets try to analyze data for sense/antisense with their controls
# HEK-WT cells +pCas9-NBN +HEK site4 +dsODN (+) -> pWT_site4_ps
# HEK-WT cells, BLANK cells+dsODN (+) -> pBLANK_ps

# HEK-WT cells +pCas9-NBN +HEK site4 +dsODN (-) -> pWT_site4_ng
# HEK-WT cells, BLANK cells+dsODN (-) -> pBLANK_ng

