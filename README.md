This repository contains code used to perform GUIDE-seq analysis for 
["CRISPR-Cas9 fusion to POLD3 and timing the editing to S cell cycle phase improve homology-directed repair" (DOI: 10.7554/eLife.75415)](https://elifesciences.org/articles/75415).

License for this repository and all its files is open-source Apache License 2.0, see LICENSE file.  



See ".gitignore" to understand which files are not uploaded (due to their size). You have to put your fastq.gz
files into fastq directory. However, .RDS files inside "offtargets" folder are provided and they contain,
results from the GUIDE-seq analysis from the original GUIDE-seq package, generated with "scripts/1_guideseq_analysis.R".
